import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var purpleHelicopterOutlet: UIButton!
    @IBOutlet weak var grayHelicopterOutlet: UIButton!
    @IBOutlet weak var greenTankOutlet: UIButton!
    @IBOutlet weak var purpleTankOutlet: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    var transportChosen: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = ""
        purpleHelicopterOutlet.isHidden = true
        grayHelicopterOutlet.isHidden = true
        loadSettings()
        registerForKeyboardNotifications()
        nameTextField.delegate = self
        
    }
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    }
    
    @IBAction func didChangeSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            purpleTankOutlet.isHidden = false
            greenTankOutlet.isHidden = false
            purpleHelicopterOutlet.isHidden = true
            grayHelicopterOutlet.isHidden = true
            
        } else if sender.selectedSegmentIndex == 1 {
            purpleTankOutlet.isHidden = true
            greenTankOutlet.isHidden = true
            purpleHelicopterOutlet.isHidden = false
            grayHelicopterOutlet.isHidden = false
        }
    }
    @IBAction func firstHelicopterPlayer(_ sender: UIButton) {
        self.transportChosen = "SecondHelicopterImage"
        
        if purpleHelicopterOutlet.isTouchInside {
            self.purpleHelicopterOutlet.backgroundColor = .opaqueSeparator
            self.grayHelicopterOutlet.backgroundColor = .none
        }
    }
    
    @IBAction func secondHelicopterPlayer(_ sender: UIButton) {
        self.transportChosen = "HelicopterImage"
        
        if grayHelicopterOutlet.isTouchInside {
            self.grayHelicopterOutlet.backgroundColor = .opaqueSeparator
            self.purpleHelicopterOutlet.backgroundColor = .none
        }
        
    }
    
    @IBAction func firstTankPlayer(_ sender: UIButton) {
        self.transportChosen = "PlayerTankImage"
        
        if purpleTankOutlet.isTouchInside {
            self.purpleTankOutlet.backgroundColor = .opaqueSeparator
            self.greenTankOutlet.backgroundColor = .none
        }
    }
    
    @IBAction func secondTankPlayer(_ sender: UIButton) {
        self.transportChosen = "SecondPlayerTankImage"
        
        if greenTankOutlet.isTouchInside {
            self.greenTankOutlet.backgroundColor = .opaqueSeparator
            self.purpleTankOutlet.backgroundColor = .none
        }
    }
    
    @IBAction func quitSettings(_ sender: UIButton) {
        saveSettings()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func saveSettings() {
        guard let name = nameTextField.text else {return}
        let element = GameSettings(transportColor: transportChosen, userName: name)
        SettingsManager.shared.saveSettings(element)
    }
    
    func loadSettings() {
        let loadSettings = SettingsManager.shared.loadSettings()
        
        if let name = loadSettings.userName {
            nameTextField.text = name
        }
    }
    
    
    
}
extension SettingsViewController: UITextFieldDelegate  {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        nameTextField.resignFirstResponder()
        return true
    }
}


