import Foundation
import UIKit

enum SettingsKeys: String {
    case gameSettings
}

class SettingsManager {
    static let shared = SettingsManager()
    
    private init() {}
    
    func saveSettings(_ settings: GameSettings) {
        UserDefaults.standard.set(encodable: settings, forKey: SettingsKeys.gameSettings.rawValue)
    }
    
    func loadSettings() -> GameSettings {
        
        guard let settings = UserDefaults.standard.value(GameSettings.self, forKey: SettingsKeys.gameSettings.rawValue) else {
            return GameSettings(transportColor: nil, userName: nil)
        }
        return settings
    }
    
}


