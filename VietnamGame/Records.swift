
import Foundation

class Records: Codable {
    var playerName: String?
    var score: Int?
    var data: String?
    
    init(playerName: String?, score: Int, data: String) {
        self.playerName = playerName
        self.score = score
        self.data = data
    }
    
    enum Keys: String, CodingKey {
        case playerName
        case score
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        playerName = try container.decodeIfPresent(String.self, forKey: .playerName)
        score = try container.decodeIfPresent(Int.self, forKey: .score)
        data = try container.decodeIfPresent(String.self, forKey: .data)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(self.playerName, forKey: .playerName)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.data, forKey: .data)
    }
    
}



