import Foundation

enum RecordsKeys: String {
    case records
}

class RecordsManager {
    static let shared = RecordsManager()
    
    func saveRecords(_ records: Records) {
        
        var recordsArray = self.loadRecords()
        recordsArray.append(records)
        UserDefaults.standard.set(encodable: recordsArray, forKey: RecordsKeys.records.rawValue)
    }
    
    func loadRecords() -> [Records] {
        guard let records = UserDefaults.standard.value([Records].self, forKey: RecordsKeys.records.rawValue) else {
            return []
        }
        return records
    }
    
    func deleteRecords() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: RecordsKeys.records.rawValue)
        }
    }
    
}
