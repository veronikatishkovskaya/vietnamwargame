import UIKit
import SpriteKit

class StartGameViewController: UIViewController {
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var highScoreButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var tapLabel: UILabel!
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var sceneView: SKView!
    
    var scene: TankScene?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isAppAlreadyLaunchedOnce()
        createTankScene()
        setMenuSettings()
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.sceneView)
        
        if let scene = self.scene {
            scene.moveHelicopter()
        }
    }
    
    @IBAction func startGame(_ sender: UIButton) {
        highScoreButton.backgroundColor = #colorLiteral(red: 0.3692261577, green: 0.5022545457, blue: 0.177287221, alpha: 1)
        highScoreButton.isEnabled = true
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func goToHighScore(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "HighScoreViewController") as! HighScoreViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        startGameButton.backgroundColor = #colorLiteral(red: 0.3692261577, green: 0.5022545457, blue: 0.177287221, alpha: 1)
        startGameButton.isEnabled = true
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func isAppAlreadyLaunchedOnce() -> Bool {
        let defaults = UserDefaults.standard
        
        if defaults.string(forKey: "isAppAlreadyLaunchedOnce") != nil {
            return true
        } else {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            startGameButton.backgroundColor = .gray
            startGameButton.isEnabled = false
            highScoreButton.backgroundColor = .gray
            highScoreButton.isEnabled = false
            return false
        }
    }
    
    func createTankScene() {
        self.scene = TankScene(size: CGSize(width: self.sceneView.frame.size.width, height: self.sceneView.frame.size.height))
        self.scene?.backgroundColor = #colorLiteral(red: 0.3416689038, green: 0.4354701042, blue: 0.2860019207, alpha: 1)
        self.sceneView.backgroundColor = #colorLiteral(red: 0.3416689038, green: 0.4354701042, blue: 0.2860019207, alpha: 1)
        self.sceneView.presentScene(scene)
    }
    
    func setMenuSettings() {
        startGameButton.titleLabel?.font = UIFont(name: "Orbitron", size: 15)
        startGameButton.shadowAppear()
        highScoreButton.titleLabel?.font = UIFont(name: "Orbitron", size: 15)
        highScoreButton.shadowAppear()
        settingsButton.titleLabel?.font = UIFont(name: "Orbitron", size: 15)
        settingsButton.shadowAppear()
        gameLabel.font = UIFont(name: "Orbitron", size: 40)
        gameLabel.shadowColor = .brown
        gameLabel.shadowOffset = CGSize(width: 4.0, height: -1.0)
        tapLabel.font = UIFont(name: "Orbitron", size: 15)
        
    }
    
    
}

extension UIView {
    func shadowAppear(_ radius: CGFloat = 15) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 10, height: 20)
        layer.shadowRadius = 10
        layer.cornerRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
    }
}

