import UIKit
import SpriteKit
import GameplayKit

class TankScene: SKScene {
    
    var helicopterFrame:[SKTexture]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        var frames:[SKTexture] = []
        
        let helicopterAtlas = SKTextureAtlas(named: "Helicopters")
        
        for index in 1...4 {
            let textureName = "helic_\(index)"
            let texture = helicopterAtlas.textureNamed(textureName)
            frames.append(texture)
        }
        
        self.helicopterFrame = frames
    }
    
    func moveHelicopter() {
        let texture = self.helicopterFrame![0]
        let helicopter = SKSpriteNode(texture: texture)
        
        helicopter.size = CGSize(width: 280, height: 100)
        
        let randomPosition = GKRandomDistribution(lowestValue: 50, highestValue: Int(self.frame.size.height - 50))
        let yPosition = CGFloat(randomPosition.nextInt())
        
        let rightToLeft = arc4random() % 2 == 0
        let xPosition = rightToLeft ? self.frame.size.width + helicopter.size.width / 2 : -helicopter.size.width / 2
        helicopter.position = CGPoint(x: xPosition, y: yPosition)
        
        if rightToLeft {
            helicopter.xScale = -1
        }
        self.addChild(helicopter)
        helicopter.run(SKAction.repeatForever(SKAction.animate(with: self.helicopterFrame!, timePerFrame: 0.05, resize: false, restore: true)))
        
        var distanceToCover = self.frame.size.width + helicopter.size.width
        
        if rightToLeft {
            distanceToCover *= -1
        }
        
        let time = TimeInterval(abs(distanceToCover / 140))
        
        let moveAction = SKAction.moveBy(x: distanceToCover, y: 0, duration: time)
        let removeAction = SKAction.run {
            helicopter.removeAllActions()
            helicopter.removeFromParent()
        }
        
        let allActions = SKAction.sequence([moveAction, removeAction])
        helicopter.run(allActions)
        
    }
    
    
}
