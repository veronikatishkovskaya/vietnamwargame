import UIKit
import CoreMotion

class ViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var rightSideView: UIImageView!
    @IBOutlet weak var centralView: UIImageView!
    @IBOutlet weak var leftSideView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var gameOverView: UIView!
    @IBOutlet weak var playerLabel: UILabel!
    
    //MARK: - let
    let helicopterView = UIImageView()
    let lakeView = UIImageView()
    let bushView = UIImageView()
    let tankView = UIImageView()
    let smallRockView = UIImageView()
    let screenHeight = Int(UIScreen.main.bounds.height)
    let screenWidth = Int(UIScreen.main.bounds.width)
    
    //MARK: - var
    var timer = Timer()
    var scoreTimer = Timer()
    var scoreDisplayed = 0
    var dangerObjectsArray: [UIImageView] = []
    var allObjectsArray: [UIImageView] = []
    var motionManager = CMMotionManager()
    
    //MARK: - VC Life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackground()
        createNewBackground()
        createleftRockWall()
        createNewLeftRockWall()
        createRightRockWall()
        createNewRightRockWall()
        timerForTank()
        timerForLake()
        timerForBushes()
        timerForSmallRocks()
        createScoreTimer()
        setupDefaultSettings()
        createHelicopter()
        helicopterIntersectsObjects()
        helicopterMoveByAccelerometer()
        gameOverView.isHidden = true
    }
    
    //MARK: - IBAction
    @IBAction func quitGame(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moveLeft(_ sender: UIButton) {
        UIImageView.animate(withDuration: 0.5) {
            self.helicopterIntersectsRocks()
            self.helicopterView.frame.origin.x -= 20
        }
    }
    
    @IBAction func moveRight(_ sender: UIButton) {
        UIImageView.animate(withDuration: 0.5) {
            self.helicopterIntersectsRocks()
            self.helicopterView.frame.origin.x += 20
        }
    }
    
    @IBAction func goToHighScore(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "HighScoreViewController") as! HighScoreViewController
        passGameScore()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func startAgainGame(_ sender: UIButton) {
        let startAgain = self.storyboard?.instantiateViewController(withIdentifier: "StartGameViewController") as! StartGameViewController
        passGameScore()
        self.navigationController?.pushViewController(startAgain, animated: true)
    }
    
    //MARK: - Life Func
    
    func createleftRockWall() {
        let leftRockView = UIImageView()
        leftRockView.frame = CGRect(x: 0,
                                    y: 0,
                                    width: Int(leftSideView.frame.width),
                                    height: self.screenHeight)
        leftRockView.image = UIImage(named: "LeftRocksImage")
        leftRockView.contentMode = .scaleAspectFill
        self.leftSideView.addSubview(leftRockView)
        self.animatedLeftRocks(view: leftRockView)
    }
    
    func createNewLeftRockWall() {
        let newLeftRockView = UIImageView()
        newLeftRockView.frame = CGRect(x: self.leftSideView.frame.origin.x,
                                       y: -(CGFloat(screenHeight)),
                                       width: leftSideView.frame.width,
                                       height: CGFloat(screenHeight))
        newLeftRockView.image = UIImage(named: "LeftRocksImage")
        newLeftRockView.contentMode = .scaleAspectFill
        self.leftSideView.addSubview(newLeftRockView)
        self.animatedLeftRocks(view: newLeftRockView)
    }
    
    func animatedLeftRocks(view: UIView) {
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear) {
            view.frame.origin.y += CGFloat(self.screenHeight)
        } completion: { (_) in
            if view.frame.origin.y < CGFloat(self.screenHeight) {
                self.animatedLeftRocks(view: view)
            } else {
                self.createNewLeftRockWall()
                view.removeFromSuperview()
            }
        }
    }
    
    func createRightRockWall() {
        let rightRockView = UIImageView()
        rightRockView.frame = CGRect(x: CGFloat(self.screenWidth) - rightSideView.frame.width,
                                     y: self.rightSideView.frame.origin.y,
                                     width: rightSideView.frame.size.width,
                                     height: CGFloat(self.screenHeight))
        rightRockView.image = UIImage(named: "RightRocksImage")
        rightRockView.contentMode = .scaleAspectFill
        self.rightSideView.addSubview(rightRockView)
        self.animatedRightRocks(view: rightRockView)
    }
    
    func createNewRightRockWall() {
        let newRightRockView = UIImageView()
        newRightRockView.frame = CGRect(x: CGFloat(self.screenWidth) - rightSideView.frame.width,
                                        y: -(CGFloat(screenHeight)),
                                        width: self.rightSideView.frame.size.width,
                                        height: CGFloat(self.screenHeight))
        newRightRockView.image = UIImage(named: "RightRocksImage")
        newRightRockView.contentMode = .scaleAspectFill
        self.rightSideView.addSubview(newRightRockView)
        self.animatedRightRocks(view: newRightRockView)
    }
    
    func animatedRightRocks(view: UIView) {
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear) {
            view.frame.origin.y += CGFloat(self.screenHeight)
        } completion: { (_) in
            if view.frame.origin.y < CGFloat(self.screenHeight) {
                self.animatedRightRocks(view: view)
            } else {
                self.createNewRightRockWall()
                view.removeFromSuperview()
            }
        }
    }
    
    func createBackground() {
        let backgroundView = UIImageView()
        backgroundView.frame = CGRect(x: 0,
                                      y: 0,
                                      width: screenWidth,
                                      height: screenHeight)
        backgroundView.image = UIImage(named: "BackgroundImage")
        backgroundView.contentMode = .scaleAspectFill
        self.centralView.insertSubview(backgroundView, belowSubview: centralView)
        self.animatedNewBackground(view: backgroundView)
    }
    
    func createNewBackground() {
        let newBackgroundView = UIImageView()
        newBackgroundView.frame = CGRect(x: 0,
                                         y: -(CGFloat(screenHeight)),
                                         width: CGFloat(screenWidth),
                                         height: CGFloat(screenHeight))
        newBackgroundView.image = UIImage(named: "BackgroundImage")
        newBackgroundView.contentMode = .scaleAspectFill
        self.centralView.insertSubview(newBackgroundView, belowSubview: centralView)
        self.animatedNewBackground(view: newBackgroundView)
    }
    
    func animatedNewBackground(view: UIView) {
        UIView.animate(withDuration: 5, delay: 0, options: .curveLinear) {
            view.frame.origin.y += CGFloat(self.screenHeight)
        } completion: { (_) in
            if view.frame.origin.y < CGFloat(self.screenHeight) {
                self.animatedNewBackground(view: view)
            } else {
                self.createNewBackground()
                view.removeFromSuperview()
            }
        }
    }
    
    func createBushView() {
        let bushView = UIImageView()
        bushView.frame = CGRect(x: self.randPosition(),
                                y: -screenHeight + 200,
                                width: 40,
                                height: 40)
        bushView.image = UIImage(named: "BushesImage")
        bushView.contentMode = .scaleAspectFill
        self.view.insertSubview(bushView, aboveSubview: centralView)
        self.animatedObjects(view: bushView)
        allObjectsArray.append(bushView)
    }
    
    func animatedObjects(view: UIView) {
        UIView.animate(withDuration: 5.0, delay: 0, options: .curveLinear) {
            view.frame.origin.y += CGFloat(self.screenHeight)
        } completion: { (_) in
            if view.frame.origin.y > CGFloat(self.screenHeight) {
                view.removeFromSuperview()
            } else {
                self.animatedObjects(view: view)
            }
        }
    }
    
    func timerForBushes() {
        let interval = 5.0
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            self.createBushView()
            
        }
        timer.fire()
    }
    
    func createSmallRocks() {
        let smallRockView = UIImageView()
        smallRockView.frame = CGRect(x: self.randPosition(),
                                     y: -screenHeight + 100,
                                     width: 40,
                                     height: 40)
        smallRockView.image = UIImage(named: "SmallRockImage")
        smallRockView.contentMode = .scaleAspectFill
        self.view.insertSubview(smallRockView, aboveSubview: centralView)
        self.animatedObjects(view: smallRockView)
        dangerObjectsArray.append(smallRockView)
        allObjectsArray.append(smallRockView)
    }
    
    func timerForSmallRocks() {
        let interval = 5.0
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            self.createSmallRocks()
        }
        timer.fire()
    }
    
    func createTank() {
        let tankView = UIImageView()
        tankView.frame = CGRect(x: self.randPosition(),
                                y: -screenHeight,
                                width: 60,
                                height: 100)
        tankView.image = UIImage(named: "TankImage")
        tankView.contentMode = .scaleAspectFill
        self.view.insertSubview(tankView, belowSubview: helicopterView)
        self.animatedTank(view: tankView)
        dangerObjectsArray.append(tankView)
        allObjectsArray.append(tankView)
    }
    
    func timerForTank() {
        let interval = 5.0
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            self.createTank()
        }
        timer.fire()
    }
    
    func animatedTank(view: UIView) {
        UIView.animate(withDuration: 4.0, delay: 0, options: .curveLinear) {
            view.frame.origin.y += CGFloat(self.screenHeight)
        } completion: { (_) in
            if view.frame.origin.y > CGFloat(self.screenHeight) {
                view.removeFromSuperview()
            } else {
                self.animatedTank(view: view)
            }
        }
    }
    
    func createLake() {
        let lakeView = UIImageView()
        lakeView.frame = CGRect(x: self.randPosition(),
                                y: -screenHeight + 300,
                                width: 100,
                                height: 90)
        lakeView.image = UIImage(named: "LakeImage")
        lakeView.contentMode = .scaleAspectFill
        self.view.insertSubview(lakeView, aboveSubview: centralView)
        self.animatedObjects(view: lakeView)
        allObjectsArray.append(lakeView)
    }
    
    func timerForLake() {
        let interval = 5.0
        let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            self.createLake()
        }
        timer.fire()
    }
    
    func createHelicopter() {
        helicopterView.frame = CGRect(x: self.centralView.frame.width / 2 + 30,
                                      y: 650,
                                      width: 30,
                                      height: 100)
        let playerImage = SettingsManager.shared.loadSettings()
        if let player = playerImage.transportColor {
            helicopterView.image = UIImage(named: player)
        }
        helicopterView.contentMode = .scaleAspectFill
        self.view.addSubview(helicopterView)
        
    }
    
    func helicopterIntersectsRocks() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            if let leftFrame = self.leftSideView.layer.presentation()?.frame {
                if leftFrame.intersects(self.helicopterView.frame) {
                    self.stopGame()
                }
            }
            if let rightFrame = self.rightSideView.layer.presentation()?.frame {
                if rightFrame.intersects(self.helicopterView.frame) {
                    self.stopGame()
                }
            }
        }
    }
    
    func helicopterIntersectsObjects() {
        let interval = 0.01
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            for object in self.dangerObjectsArray {
                if let objectFrame = object.layer.presentation()?.frame {
                    if self.helicopterView.frame.intersects(objectFrame) {
                        self.stopGame()
                    }
                }
            }
        }
    }
    
    func objectsIntersection() {
        let interval = 0.01
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { (timer) in
            for object in self.allObjectsArray {
                if let objectFrame = object.layer.presentation()?.frame {
                    if self.bushView.frame.intersects(objectFrame) {
                        self.bushView.removeFromSuperview()
                    } else if self.smallRockView.frame.intersects(objectFrame) {
                        self.smallRockView.removeFromSuperview()
                    } else if self.tankView.frame.intersects(objectFrame) {
                        self.tankView.removeFromSuperview()
                    }else if self.lakeView.frame.intersects(objectFrame) {
                        self.lakeView.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    func createScoreTimer() {
        self.scoreLabel.text = "0"
        scoreTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {(scoreTimer) in
            self.scoreDisplayed += 1
            self.scoreLabel.text = "\(self.scoreDisplayed)"
        }
    }
    
    func passGameScore() {
        let gameScore = self.storyboard?.instantiateViewController(withIdentifier: "HighScoreViewController") as! HighScoreViewController
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm"
        let string = formatter.string(from: date)
        let result = "\(playerLabel.text ?? ""), your game score: \(scoreDisplayed), \(string)"
        gameScore.result = result
        let score = Records(playerName: playerLabel.text, score: scoreDisplayed, data: string)
        RecordsManager.shared.saveRecords(score)
    }
    
    func stopGame() {
        gameOverView.isHidden = false
        self.view.insertSubview(self.gameOverView, aboveSubview: self.helicopterView)
        scoreTimer.invalidate()
        self.helicopterView.removeFromSuperview()
    }
    
    func randPosition () -> (Int) {
        let xPosition = Int.random(in: Int(self.leftSideView.frame.size.width)...Int(self.centralView.frame.size.width - 50))
        return xPosition
    }
    
    func setupDefaultSettings() {
        let playerName = SettingsManager.shared.loadSettings()
        if let name = playerName.userName {
            playerLabel.text = name
        } else if playerName.userName == "" {
            playerLabel.text = "User Name"
        }
        helicopterView.frame = CGRect(x: self.centralView.frame.width / 2 + 30,
                                      y: 650,
                                      width: 30,
                                      height: 100)
        helicopterView.image = UIImage(named: "HelicopterImage" )
        helicopterView.contentMode = .scaleAspectFill
        self.view.addSubview(helicopterView)
    }
    
    func helicopterMoveByAccelerometer() {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.03
            motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    if acceleration.x > 0.02 {
                        UIImageView.animate(withDuration: 0.4, delay: 0, options: .curveLinear) {
                            self?.helicopterIntersectsRocks()
                            self?.helicopterView.frame.origin.x += 5
                        }
                    } else if acceleration.x < -0.02 {
                        UIImageView.animate(withDuration: 0.4, delay: 0, options: .curveLinear) {
                            self?.helicopterIntersectsRocks()
                            self?.helicopterView.frame.origin.x -= 5
                        }
                    }
                }
            }
        }
    }
    
    func jumpHelicopterByGyro() {
        if motionManager.isDeviceMotionAvailable {
            motionManager.gyroUpdateInterval = 0.5
            motionManager.startGyroUpdates(to: .main) { (data, error) in
                if let rate = data?.rotationRate {
                    print("x = " + "\(rate.x)")
                    print("y = " + "\(rate.y)")
                    print("z = " +  "\(rate.z)")
                    print()
                    
                }
            }
        }
    }
    
}

