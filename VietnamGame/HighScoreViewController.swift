import UIKit

class HighScoreViewController: UIViewController {
    @IBOutlet weak var quitHighScoreLabel: UIButton!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clearAllButton: UIButton!
    var result = ""
    var recordsArray: [String] = []
    var gameResultArray: [Records] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRecords()
        quitHighScoreLabel.titleLabel?.font = UIFont(name: "Orbitron", size: 15)
        clearAllButton.titleLabel?.font = UIFont(name: "Orbitron", size: 15)
        highScoreLabel.font = UIFont(name: "Orbitron", size: 30)
        
    }
    
    @IBAction func clearAllRecords(_ sender: UIButton) {
        RecordsManager.shared.deleteRecords()
        UserDefaults.standard.synchronize()
        gameResultArray.removeAll()
        tableView.reloadData()
    }
    
    @IBAction func quitHighScore(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func loadRecords() {
        let resultsToArray = RecordsManager.shared.loadRecords()
        gameResultArray.append(contentsOf: resultsToArray)
        gameResultArray.sort {$0.score! > $1.score!}
        print(gameResultArray)
    }
    
    
}
extension HighScoreViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameResultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(object: gameResultArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
}
