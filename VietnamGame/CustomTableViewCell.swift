import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var scoreResults: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(object: Records) {
        scoreResults.font = UIFont(name: "Orbitron", size: 15)
        scoreResults.text = "   \(object.playerName ?? ""), your score is \(object.score ?? 0); \(object.data ?? "")"
    }
}
