
import Foundation

class GameSettings: Codable {
    var transportColor: String?
    var userName: String?
    
    
    init(transportColor: String?, userName: String?) {
        self.transportColor = transportColor
        self.userName = userName
    }
    
    enum Keys: String, CodingKey {
        case transportColor
        case userName
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: Keys.self)
        
        transportColor = try container.decodeIfPresent(String.self, forKey: .transportColor)
        userName = try container.decodeIfPresent(String.self, forKey: .userName)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        
        try container.encode(self.transportColor, forKey: .transportColor)
        try container.encode(self.userName, forKey: .userName)
    }
    
    
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
    
}
